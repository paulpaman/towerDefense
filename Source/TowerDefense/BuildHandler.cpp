// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildHandler.h"
#include "Tower.h"
#include "TowerData.h"
// Sets default values
ABuildHandler::ABuildHandler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildHandler::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABuildHandler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABuildHandler::SpawnTower(FTransform NodeTransform, bool& bHasSpawned, ATowerNode* TowerNode, TSubclassOf<ATower>TowerToSpawn, int32 TowerCost)
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	if (Gold >= TowerCost)
	{
		ATower* Tower = GetWorld()->SpawnActor<ATower>(TowerToSpawn, NodeTransform, spawnParams);
		Gold -= TowerCost;
		Tower->TotalCost = TowerCost;
		Tower->nodeOwner = TowerNode;
		bHasSpawned = true;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Not Enough Gold!"));
		bHasSpawned = false;
	}
}

void ABuildHandler::AddGold(int32 goldToAdd)
{
	Gold += goldToAdd;
}

void ABuildHandler::UpgradeTower(ATower* Tower, bool& bHasUpgraded)
{
	UpgradeCost = TowerData->TowerUpgradeCost * TowerData->UpgradeCostMultiplier[Tower->CurrentLevel - 1];
	if (Gold >= UpgradeCost)
	{
		Tower->Upgrade();
		Gold -= UpgradeCost;
		Tower->TotalCost += UpgradeCost;
		bHasUpgraded = true;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Not Enough Gold!"));
		bHasUpgraded = false;
	}
}

void ABuildHandler::Refund(ATower* Tower)
{
	Gold += Tower->TotalCost / 2;
}

void ABuildHandler::GetUpgradeCost(ATower* Tower,int32& Cost)
{
	UpgradeCost = TowerData->TowerUpgradeCost * TowerData->UpgradeCostMultiplier[Tower->CurrentLevel - 1];
	Cost = UpgradeCost;
}

