// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "HomingProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AHomingProjectile : public AProjectile
{
	GENERATED_BODY()
	
protected:
	AHomingProjectile();

	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

public:
};
