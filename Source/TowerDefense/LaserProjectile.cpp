// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserProjectile.h"
#include "BurningComponent.h"
#include "EnemyAI.h"
ALaserProjectile::ALaserProjectile()
{

}

void ALaserProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ALaserProjectile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("EnemyHit!"));
		if (UBurningComponent* burningComponent = enemy->FindComponentByClass<UBurningComponent>())
		{
			burningComponent->ResetDuration(BuffDuration);
			this->Destroy();
		}
		else
		{
			UBurningComponent* burningObj = NewObject<UBurningComponent>(enemy, BuffToAdd, "Buff");
			burningObj->SetDamagePerTick(Damage);
			burningObj->SetDuration(BuffDuration);
			enemy->AddInstanceComponent(burningObj);
			burningObj->RegisterComponent();
			this->Destroy();
		}
		
	}
}
