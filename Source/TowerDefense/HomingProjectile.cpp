// Fill out your copyright notice in the Description page of Project Settings.


#include "HomingProjectile.h"
#include "Components/CapsuleComponent.h"
#include "Projectile.h"
#include "EnemyAI.h"
#include "HealthComponent.h"

AHomingProjectile::AHomingProjectile()
{

}

void AHomingProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHomingProjectile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("EnemyHit!"));
		if (UHealthComponent* healthComponent = enemy->FindComponentByClass<UHealthComponent>())
		{
			healthComponent->TakeDamage(Damage);
		}
		this->Destroy();
	}
}

