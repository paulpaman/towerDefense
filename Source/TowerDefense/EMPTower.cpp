// Fill out your copyright notice in the Description page of Project Settings.


#include "EMPTower.h"
#include "EnemyAI.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"

AEMPTower::AEMPTower()
{

}

void AEMPTower::BeginPlay()
{
	Super::BeginPlay();
}

void AEMPTower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		enemy->GetCharacterMovement()->MaxWalkSpeed *= SlowMultiplier;
	}
}

void AEMPTower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		enemy->GetCharacterMovement()->MaxWalkSpeed /= SlowMultiplier;
	}
}

void AEMPTower::Upgrade()
{
	Super::Upgrade();
	SlowMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	SphereRadiusMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	AttackRange->SetSphereRadius(AttackRange->GetScaledSphereRadius() * SphereRadiusMultiplier);
	CurrentLevel++;
}
