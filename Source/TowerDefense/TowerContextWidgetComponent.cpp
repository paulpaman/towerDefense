// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerContextWidgetComponent.h"
#include "Tower.h"

UTowerContextWidgetComponent::UTowerContextWidgetComponent()
{
	// Set common defaults when using widgets on Actors
	SetDrawAtDesiredSize(true);
	SetWidgetSpace(EWidgetSpace::Screen);
	SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void UTowerContextWidgetComponent::InitWidget()
{
	Super::InitWidget();
}

void UTowerContextWidgetComponent::SetOwningTower(ATower* towerOwner)
{
	OwningTower = towerOwner;
}
