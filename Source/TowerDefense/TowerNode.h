// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FNodeSignature, class ATowerNode*, TowerNode);
UCLASS()
class TOWERDEFENSE_API ATowerNode : public AActor
{
	GENERATED_BODY()
private:
	

protected:
	ATowerNode();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* NodeMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;

	UFUNCTION()
		void CustomOnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
		void CustomOnEndMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
		void CustomOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION()
		void CustomOnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* BuildableMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* OriginalMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* NotBuildableMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBuildManager* BuildManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* TowerSpawn;

	UPROPERTY(BlueprintAssignable)
		FNodeSignature OnNodeClick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool canSpawnTower;

	UFUNCTION(BlueprintImplementableEvent)
		void OnClickedEvent(ATowerNode* TowerNode);


public:	



};
