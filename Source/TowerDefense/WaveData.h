// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float spawnInterval;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 TotalSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 LevelCompletionGold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float EnemyHealthMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
		TArray<TSubclassOf<class AEnemyAI>> EnemiesToSpawn;
};
