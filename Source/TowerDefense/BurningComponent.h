// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffAPI.h"
#include "BurningComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UBurningComponent : public UBuffAPI
{
	GENERATED_BODY()
protected:
	UBurningComponent();
	void BeginPlay() override;
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void RemoveBuff() override;

	UFUNCTION()
		void ApplyDamage();

		
public:
	UFUNCTION()
		void SetDamagePerTick(float damage);

	UFUNCTION()
		void ResetDuration(float duration);

	UFUNCTION()
		void SetDuration(float duration);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamagePerTick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Duration;

};
