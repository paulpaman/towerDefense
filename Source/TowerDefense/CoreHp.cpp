// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreHp.h"

// Sets default values
ACoreHp::ACoreHp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACoreHp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACoreHp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACoreHp::AddDamage(float damage)
{
	Health -= damage;
	if (Health <= 0)
	{
		OnDeath.Broadcast();
	}
}

