// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Towers")
		TSubclassOf<class ATower> Turret;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Towers")
		TSubclassOf<class ATower> SalvagerTower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Towers")
		TSubclassOf<class ATower> EMPTower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Towers")
		TSubclassOf<class ATower> PowerGeneratorTower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Towers")
		TSubclassOf<class ATower> LaserTower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tower Cost")
		int32 TurretCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tower Cost")
		int32 SalvagerTowerCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tower Cost")
		int32 EMPTowerCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tower Cost")
		int32 PowerGeneratorTowerCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tower Cost")
		int32 LaserTowerCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Upgrade Cost")
		int32 TowerUpgradeCost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Upgrade Cost")
		TArray<float> UpgradeCostMultiplier;


};
