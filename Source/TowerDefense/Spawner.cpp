// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "EnemyAI.h"
#include "WaveData.h"
// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnerMesh"));
	SetRootComponent(SpawnerMesh);
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	//SpawnWave();
	//SpawnEnemy();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASpawner::EnemyDeath(AEnemyAI* enemy)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SpawnEvent!"));
	OnEnemyDeath.Broadcast(this);
}

void ASpawner::SpawnDelay(float spawnInterval)
{
	//GetWorldTimerManager().SetTimer(timerHandle, FTimerDelegate::CreateUObject(this, &ASpawner::SpawnEnemy), spawnInterval, false);
}

void ASpawner::SpawnEnemy(float enemyHealthMultiplier)
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	AEnemyAI* enemy = GetWorld()->SpawnActor<AEnemyAI>(EnemyToSpawn, this->GetTransform(), spawnParams);
	enemy->IncreaseMaxHealth(enemyHealthMultiplier);
	int32 randomInteger = FMath::RandRange(0, WaypointsToUse.Num() - 1);
	FWaypointConfig waypointConfig = WaypointsToUse[randomInteger];
	enemy->Waypoints = waypointConfig.Waypoints;
	enemy->MoveToWaypoints();
	enemy->OnDeath.AddDynamic(this, &ASpawner::EnemyDeath);
}

