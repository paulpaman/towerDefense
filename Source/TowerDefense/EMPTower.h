// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "EMPTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEMPTower : public ATower
{
	GENERATED_BODY()
protected:
	AEMPTower();

	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void Upgrade();

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SlowMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SphereRadiusMultiplier;
};
