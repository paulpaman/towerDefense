// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "InputCoreTypes.h"
#include "BuildManager.h"
#include "Components/ArrowComponent.h"
// Sets default values
ATowerNode::ATowerNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	NodeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("NodeMesh"));
	NodeMesh->SetupAttachment(SceneComponent);
	NodeMesh->OnBeginCursorOver.AddDynamic(this, &ATowerNode::CustomOnBeginMouseOver);
	NodeMesh->OnEndCursorOver.AddDynamic(this, &ATowerNode::CustomOnEndMouseOver);
	NodeMesh->OnClicked.AddDynamic(this, &ATowerNode::CustomOnMouseClicked);
	NodeMesh->OnReleased.AddDynamic(this, &ATowerNode::CustomOnMouseReleased);

	TowerSpawn = CreateDefaultSubobject<UArrowComponent>(TEXT("TowerSpawn"));
	TowerSpawn->SetupAttachment(RootComponent);

	BuildableMaterial = CreateDefaultSubobject<UMaterial>(TEXT("BuildableMaterial"));
	NotBuildableMaterial = CreateDefaultSubobject<UMaterial>(TEXT("NotBuildableMaterial"));
	OriginalMaterial = CreateDefaultSubobject<UMaterial>(TEXT("OriginalMaterial"));

	BuildManager = CreateDefaultSubobject<UBuildManager>(TEXT("BuildManager"));
	this->AddOwnedComponent(BuildManager);
}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();
	canSpawnTower = true;

	//OnNodeClick.AddDynamic(this, &ATowerNode::OnClickedEvent);
}

// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATowerNode::CustomOnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("BeginMouseOver!"));
	if (canSpawnTower == true)
	{
		TouchedComponent->SetMaterial(0, BuildableMaterial);
	}
	else
	{
		TouchedComponent->SetMaterial(0, NotBuildableMaterial);
	}
}

void ATowerNode::CustomOnEndMouseOver(UPrimitiveComponent* TouchedComponent)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("EndMouseOver!"));
	TouchedComponent->SetMaterial(0, OriginalMaterial);
}

void ATowerNode::CustomOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Clicked!"));
	if (canSpawnTower == true)
	{
		OnClickedEvent(this);
		//OnNodeClick.Broadcast(this);
	}
}

void ATowerNode::CustomOnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Released!"));
}

