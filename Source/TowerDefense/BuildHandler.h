// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildHandler.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildHandler : public AActor
{
	GENERATED_BODY()
	
private:
	
protected:
	// Sets default values for this actor's properties
	ABuildHandler();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TowerData")
		class UTowerData* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gold")
		int32 Gold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 UpgradeCost;
public:	

	UFUNCTION(BlueprintCallable)
		void SpawnTower(FTransform NodeTransform, bool& bHasSpawned, ATowerNode* TowerNode ,TSubclassOf<ATower>TowerToSpawn, int32 TowerCost);

	UFUNCTION(BlueprintCallable)
		void AddGold(int32 goldToAdd);

	UFUNCTION(BlueprintCallable)
		void UpgradeTower(ATower* Tower, bool& bHasUpgraded);

	UFUNCTION(BlueprintCallable)
		void Refund(ATower* Tower);

	UFUNCTION(BlueprintCallable)
		void GetUpgradeCost(ATower* Tower,int32& Cost);
};
