// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "EnemyAI.h"
#include "BuildHandler.h"
#include "Kismet/GameplayStatics.h"
// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;

	OnEnemyDeath.AddDynamic(this,&UHealthComponent::OnDeathEvent);
}



void UHealthComponent::OnDeathEvent()
{
	AEnemyAI* enemy = Cast<AEnemyAI>(GetOwner());
	AActor* buildActor = UGameplayStatics::GetActorOfClass(GetWorld(), ABuildHandler::StaticClass());
	if (buildActor)
	{
		ABuildHandler* buildHandler = Cast<ABuildHandler>(buildActor);
		buildHandler->AddGold(enemy->GoldToGive);
	}
}

void UHealthComponent::TakeDamage(float damageTaken)
{
	CurrentHealth -= damageTaken;
	if (CurrentHealth <= 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("DEad!"));
		OnEnemyDeath.Broadcast();
	}
}

void UHealthComponent::IncreaseHealth(float multiplier)
{
	MaxHealth *= multiplier;
	CurrentHealth = MaxHealth;
}

