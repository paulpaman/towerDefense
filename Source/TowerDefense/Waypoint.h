// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Waypoint.generated.h"

UCLASS()
class TOWERDEFENSE_API AWaypoint : public AActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditAnywhere,meta = (AllowPrivateAccess = "true"))
	int32 WaypointOrder;

protected:

	AWaypoint();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* WaypointMesh;

public:
	int32 GetWaypointOrder();
	

};
