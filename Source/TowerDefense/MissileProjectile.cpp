// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileProjectile.h"
#include "Components/CapsuleComponent.h"
#include "EnemyAI.h"
#include "HealthComponent.h"
#include "Components/PrimitiveComponent.h"

AMissileProjectile::AMissileProjectile()
{
	AoeCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("AoeCollision"));
	AoeCollision->SetupAttachment(RootComponent);
}

void AMissileProjectile::BeginPlay()
{
	Super::BeginPlay();
	CapsuleCollision->OnComponentBeginOverlap.AddDynamic(this, &AMissileProjectile::OnOverlap);
}

void AMissileProjectile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	//if (UPrimitiveComponent* landscape = Cast<UPrimitiveComponent>(OverlappedComponent))
	//{
	//	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("OverlapLandscape!"));
	//	//AoeCollision->OnComponentBeginOverlap.AddDynamic(this, &AMissileProjectile::OnAoeOverlap);
	//	//ApplyAoeDamage();
	//	//this->Destroy();
	//}
}

void AMissileProjectile::ApplyAoeDamage()
{
	for (AEnemyAI* enemy : EnemiesToHit)
	{
		if (UHealthComponent* healthComponent = enemy->FindComponentByClass<UHealthComponent>())
		{
			healthComponent->TakeDamage(Damage);
		}
	}
}

void AMissileProjectile::OnAoeOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		EnemiesToHit.Add(enemy);
	}
}
