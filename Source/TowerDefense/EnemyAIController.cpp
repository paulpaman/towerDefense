// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "EnemyAI.h"

AEnemyAIController::AEnemyAIController()
{

}
void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);
	AEnemyAI* EnemyAI = Cast<AEnemyAI>(GetPawn());

	if (EnemyAI)
	{
		EnemyAI->CurrentWaypoint++;
		EnemyAI->MoveToWaypoints();
	}
}

void AEnemyAIController::DisableControlllerInput()
{
	AEnemyAIController* currentController = Cast<AEnemyAIController>(this);
	if (currentController)
	{
		currentController->StopMovement();
		currentController->UnPossess();
		currentController->Destroy();
	}
}

