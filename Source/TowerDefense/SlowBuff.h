// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuffAPI.h"
#include "SlowBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API USlowBuff : public UBuffAPI
{
	GENERATED_BODY()
protected:

	USlowBuff();
	void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SlowMultiplier;

public:
	void RemoveBuff() override;
};
