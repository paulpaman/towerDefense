// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "TowerContextWidgetComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UTowerContextWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()

protected:
	UTowerContextWidgetComponent();

	virtual void InitWidget() override;

	/* Actor that widget is attached to via WidgetComponent */
	UPROPERTY(BlueprintReadOnly, Category = "ActorWidget")
		class ATower* OwningTower;

public:

	/* Set the owning actor so widgets have access to whatever is, broadcasting OnOwningActorChanged event */
	UFUNCTION(BlueprintCallable, Category = "UI")
		void SetOwningTower(ATower* towerOwner);
	
};
