// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "SalvagerTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASalvagerTower : public ATower
{
	GENERATED_BODY()
private:
	FTimerHandle TimerHandle;
	
protected:
	ASalvagerTower();
	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void Upgrade() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 GoldToAdd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AActor* buildHandlerActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ABuildHandler* buildHandler;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ABuildHandler> buildHandlerClass;

	UFUNCTION()
		void GenerateGold();

};
