// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuildManager.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOWERDEFENSE_API UBuildManager : public UActorComponent
{
	GENERATED_BODY()

protected:
	// Sets default values for this component's properties
	UBuildManager();
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TowerData")
		class UTowerData* TowerData;
public:	
	
	UFUNCTION(BlueprintCallable)
		void SpawnTurret(FTransform NodeTransform);

		
};
