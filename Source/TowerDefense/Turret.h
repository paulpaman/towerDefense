// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "Turret.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATurret : public ATower
{
	GENERATED_BODY()
private:
	FTimerHandle TimerHandle;
protected:
	ATurret();
	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override ;
	
	void SelectNearestTarget() override;

	void Upgrade() override;

	UFUNCTION(BlueprintCallable)
		void Shoot();
	UFUNCTION(BlueprintCallable)
		void AimAtTarget();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageMultiplier;
};
