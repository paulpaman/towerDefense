// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "LaserProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserProjectile : public AProjectile
{
	GENERATED_BODY()
protected:
	ALaserProjectile();

	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class UBurningComponent> BuffToAdd;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BuffDuration;

};
