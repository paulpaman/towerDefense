// Fill out your copyright notice in the Description page of Project Settings.


#include "SalvagerTower.h"
#include "Kismet/GameplayStatics.h"
#include "BuildHandler.h"
#include "EnemyAI.h"
#include "HealthComponent.h"
ASalvagerTower::ASalvagerTower()
{

}

void ASalvagerTower::BeginPlay()
{
	Super::BeginPlay();
	buildHandlerActor = UGameplayStatics::GetActorOfClass(GetWorld(), buildHandlerClass);
	if (buildHandlerActor)
	{
		buildHandler = Cast<ABuildHandler>(buildHandlerActor);
	}

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ASalvagerTower::GenerateGold, 3.0f, true);
}

void ASalvagerTower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		enemy->HealthComponent->OnEnemyDeath.AddDynamic(this, &ASalvagerTower::GenerateGold);
	}
}

void ASalvagerTower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		enemy->HealthComponent->OnEnemyDeath.RemoveDynamic(this, &ASalvagerTower::GenerateGold);
	}
}

void ASalvagerTower::Upgrade()
{
	Super::Upgrade();
	GoldToAdd *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	CurrentLevel++;
}

void ASalvagerTower::GenerateGold()
{
	buildHandler->AddGold(GoldToAdd);
}
