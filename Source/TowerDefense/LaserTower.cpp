// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Tower.h"
#include "EnemyAI.h"
#include "Projectile.h"
#include "LaserProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "BurningComponent.h"
ALaserTower::ALaserTower()
{

}

void ALaserTower::BeginPlay()
{
	Super::BeginPlay();
}

void ALaserTower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		CurrentEnemy = enemy;
		AimAtTarget();
	}
}

void ALaserTower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
}

void ALaserTower::Upgrade()
{
	Super::Upgrade();
	BurnDamageMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	BurnDurationMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	CurrentLevel++;
}

void ALaserTower::Shoot()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	if (CurrentEnemy != nullptr)
	{
		ALaserProjectile* projectile = GetWorld()->SpawnActor<ALaserProjectile>(Projectile, ProjectileSpawn->GetComponentTransform(), spawnParams);
		projectile->Damage *= BurnDamageMultiplier;
		projectile->BuffDuration *= BurnDurationMultiplier;
		projectile->SetHomingTarget(CurrentEnemy);
	}
}

void ALaserTower::AimAtTarget()
{
	if (CurrentEnemy != nullptr)
	{
		FRotator rot = UKismetMathLibrary::FindLookAtRotation(GunMesh->GetComponentLocation(), CurrentEnemy->GetActorLocation());
		FRotator interpRot = UKismetMathLibrary::RInterpTo(GunMesh->GetComponentRotation(), rot, GetWorld()->GetDeltaSeconds(), 100.0f);
		GunMesh->SetRelativeRotation(interpRot);

		Shoot();
	}
}
