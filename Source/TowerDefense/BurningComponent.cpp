// Fill out your copyright notice in the Description page of Project Settings.


#include "BurningComponent.h"
#include "EnemyAI.h"
#include "HealthComponent.h"

UBurningComponent::UBurningComponent()
{

}

void UBurningComponent::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UBurningComponent::ApplyDamage, 1.0f, true);
}

void UBurningComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UBurningComponent::ApplyDamage()
{
	UHealthComponent* healthComponent = enemy->FindComponentByClass<UHealthComponent>();
	healthComponent->TakeDamage(DamagePerTick);

	Duration -= 1;
	if (Duration <= 0.f)
	{
		RemoveBuff();
	}

}

void UBurningComponent::SetDamagePerTick(float damage)
{
	DamagePerTick = damage;
}

void UBurningComponent::ResetDuration(float duration)
{
	Duration = duration;
}

void UBurningComponent::SetDuration(float duration)
{
	Duration = duration;
}

void UBurningComponent::RemoveBuff()
{
	Super::RemoveBuff();
}


