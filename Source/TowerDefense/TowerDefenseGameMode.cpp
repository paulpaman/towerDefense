// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerDefenseGameMode.h"
#include "Spawner.h"
#include "Kismet/GameplayStatics.h"
#include "WaveData.h"
#include "EnemyAI.h"
ATowerDefenseGameMode::ATowerDefenseGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATowerDefenseGameMode::BeginPlay()
{
	Super::BeginPlay();

	curEnemy = 0;
	CurrentWave = 0;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), Spawners);
	for (AActor* spawners : Spawners)
	{
		ASpawner* spawner = Cast<ASpawner>(spawners);
		spawner->OnEnemyDeath.AddDynamic(this, &ATowerDefenseGameMode::OnEnemiesKilled);
	}
	AddWave();
}

void ATowerDefenseGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATowerDefenseGameMode::CheckWaveProgress()
{
	if (CurrentWave >= 10)
	{
		OnLevelEnd.Broadcast(this);
	}
	else if(EnemiesKilled >= WaveData[CurrentWave]->TotalSpawns * Spawners.Num())
	{
		curEnemy = 0;
		EnemiesKilled = 0;
		CurrentWave += 1;
		if (CurrentWave <= 10)
		{
			AddWave();
		}
	}
}

void ATowerDefenseGameMode::AddEnemiesKilled(int32 numToAdd)
{
	EnemiesKilled += numToAdd;
}

void ATowerDefenseGameMode::SpawnWave()
{
	if (CurrentWave < WaveData.Num())
	{
		//AddEnemies();
	}
}

void ATowerDefenseGameMode::AddEnemies(TSubclassOf<class AEnemyAI> enemy,float enemyHealthMultiplier)
{
	for (AActor* spawners : Spawners)
	{
		ASpawner* spawner = Cast<ASpawner>(spawners);
		spawner->EnemyToSpawn = enemy;
		spawner->SpawnEnemy(enemyHealthMultiplier);
	}
}

void ATowerDefenseGameMode::OnEnemiesKilled(ASpawner* spawner)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("DeadEvent!"));
	AddEnemiesKilled(1);
	CheckWaveProgress();
}
