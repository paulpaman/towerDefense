// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "EnemyAI.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"
#include "Projectile.h"
#include "HomingProjectile.h"
#include "Components/WidgetComponent.h"
#include "TowerNode.h"
// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	SetRootComponent(BaseMesh);

	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	GunMesh->SetupAttachment(RootComponent);

	ProjectileSpawn = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileSpawn"));
	ProjectileSpawn->SetupAttachment(GunMesh);

	AttackRange = CreateDefaultSubobject<USphereComponent>(TEXT("AttackRange"));
	AttackRange->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	AttackRange->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlap);
	AttackRange->OnComponentEndOverlap.AddDynamic(this, &ATower::OnOverlapEnd);
	BaseMesh->OnClicked.AddDynamic(this, &ATower::CustomOnMouseClicked);
	CurrentLevel = 1;

	if (BaseMesh)
	{
		BaseMesh->SetMaterial(0, UpgradeMat[0]);
	}

	if (GunMesh)
	{
		GunMesh->SetMaterial(0, UpgradeMat[0]);
	}
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATower::SelectNearestTarget()
{
	float dist = 9999999.0f;
	float currentDist;
	
	if (EnemiesInRange.Num() > 0)
	{
		for (AEnemyAI* enemy : EnemiesInRange)
		{
			currentDist = FVector::Distance(this->GetActorLocation(), enemy->GetActorLocation());
			if (currentDist < dist)
			{
				CurrentEnemy = enemy;
				dist = currentDist;
			}
		}
	}
	else if (EnemiesInRange.Num() <= 0)
	{
		CurrentEnemy = nullptr;
	}
}


void ATower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("EnemyOverlap!"));
}

void ATower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("OverlapEnd!"));
}

void ATower::Upgrade()
{
	if (BaseMesh)
	{
		BaseMesh->SetMaterial(0, UpgradeMat[CurrentLevel]);
	}
	if (GunMesh)
	{
		GunMesh->SetMaterial(0, UpgradeMat[CurrentLevel]);
	}
}


void ATower::CustomOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("MouseClick!"));
	OnClickedEvent(this);
}

