// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreHp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCoreHpSignature);
UCLASS()
class TOWERDEFENSE_API ACoreHp : public AActor
{
	GENERATED_BODY()
		
protected:
	// Sets default values for this actor's properties
	ACoreHp();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Health;

	UFUNCTION(BlueprintCallable)
		void AddDamage(float damage);

	UPROPERTY(BlueprintAssignable)
		FCoreHpSignature OnDeath;

};
