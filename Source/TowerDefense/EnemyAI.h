// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyAI.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, class AEnemyAI*, enemy);

UCLASS()
class TOWERDEFENSE_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

protected:

	// Sets default values for this character's properties
	AEnemyAI();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsGround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsBoss;

	

public:	
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
		TArray<AActor*> Waypoints;

	UPROPERTY(BlueprintAssignable)
		FDeathSignature OnDeath;

	UFUNCTION()
		void MoveToWaypoints();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 CurrentWaypoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool isDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UHealthComponent* HealthComponent;

	UFUNCTION(BlueprintCallable)
		void Die();

	UFUNCTION()
		void IncreaseMaxHealth(float numMultiplier);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 GoldToGive;
};
