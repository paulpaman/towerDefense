// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserTower : public ATower
{
	GENERATED_BODY()
private:
	FTimerHandle TimerHandle;
protected:
	ALaserTower();
	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void Upgrade() override;

	UFUNCTION(BlueprintCallable)
		void Shoot();
	UFUNCTION(BlueprintCallable)
		void AimAtTarget();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BurnDamageMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BurnDurationMultiplier;
};
