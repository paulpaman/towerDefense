// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWaveSignature, class ATowerDefenseGameMode*, tDGameMode);
UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()
private:
	int32 curEnemy;
	FTimerHandle spawnTimerHandle;
protected:
	ATowerDefenseGameMode();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spawners")
		TArray<AActor*> Spawners;

	UFUNCTION(BlueprintImplementableEvent)
		void AddWave();

	UFUNCTION(BlueprintCallable)
		void CheckWaveProgress();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WaveManager")
		TArray<class UWaveData*> WaveData;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "WaveManager")
		int32 CurrentWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WaveManager")
		int32 EnemiesKilled;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WaveManager")
		TArray<class AEnemyAI*> SpawnedEnemies;

	UFUNCTION(BlueprintCallable)
		void AddEnemiesKilled(int32 numToAdd);

	UFUNCTION()
		void SpawnWave();

	UFUNCTION(BlueprintCallable)
		void AddEnemies(TSubclassOf<class AEnemyAI> enemy, float enemyHealthMultiplier);

	UPROPERTY(BlueprintAssignable)
		FWaveSignature OnLevelEnd;

	UFUNCTION()
		void OnEnemiesKilled(ASpawner* spawner);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class ACoreHp* SharedHp;


};
