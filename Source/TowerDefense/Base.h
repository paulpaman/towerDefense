// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Base.generated.h"

UCLASS()
class TOWERDEFENSE_API ABase : public AActor
{
	GENERATED_BODY()

protected:
	ABase();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* BaseMesh;

	UFUNCTION(BlueprintImplementableEvent)
		void OnTrigger(class AEnemyAI* enemy);

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class ACoreHp* SharedHp;

public:	
};
