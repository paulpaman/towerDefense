// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissileProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissileProjectile : public AProjectile
{
	GENERATED_BODY()

protected:
	AMissileProjectile();

	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
		TArray<class AEnemyAI*> EnemiesToHit;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UCapsuleComponent* AoeCollision;

	UFUNCTION()
		void ApplyAoeDamage();

	UFUNCTION()
		void OnAoeOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		
};
