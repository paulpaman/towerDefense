// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSPawnerSignature, class ASpawner*, spawner);

USTRUCT(BlueprintType)
struct FEnemyAISpawnConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
		TSubclassOf<class AEnemyAI> EnemyTypes;
};

USTRUCT(BlueprintType)
struct FWaypointConfig
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<AActor*> Waypoints;
};

UCLASS()
class TOWERDEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
private:
	FTimerHandle timerHandle;

protected:
	// Sets default values for this actor's properties
	ASpawner();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
		TArray<FEnemyAISpawnConfig> EnemySpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Waypoints")
		TArray<FWaypointConfig> WaypointsToUse;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* SpawnerMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 NumOfEnemyToSpawn;

	UFUNCTION()
		void EnemyDeath(AEnemyAI* enemy);

	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AEnemyAI> EnemyToSpawn;

	UFUNCTION(BlueprintCallable)
		void SpawnEnemy(float enemyHealthMultiplier);

	UPROPERTY(BlueprintAssignable)
		FSPawnerSignature OnEnemyDeath;

	UFUNCTION()
		void SpawnDelay(float spawnInterval);
};
