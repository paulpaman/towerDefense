// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileTower.h"
#include "Tower.h"
#include "EnemyAI.h"
#include "Projectile.h"
#include "MissileProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
AMissileTower::AMissileTower()
{

}

void AMissileTower::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle, this, &AMissileTower::AimAtTarget, AttackSpeed, true);
}

void AMissileTower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		EnemiesInRange.Add(enemy);
		if (CurrentEnemy == nullptr)
		{
			SelectNearestTarget();
		}
	}
}

void AMissileTower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		EnemiesInRange.Remove(enemy);
		SelectNearestTarget();
	}
}

void AMissileTower::SelectNearestTarget()
{
	Super::SelectNearestTarget();
}

void AMissileTower::Shoot()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	if (CurrentEnemy != nullptr)
	{
		AProjectile* projectile = GetWorld()->SpawnActor<AMissileProjectile>(Projectile, ProjectileSpawn->GetComponentTransform(), spawnParams);
	}
}

void AMissileTower::AimAtTarget()
{
	if (CurrentEnemy != nullptr)
	{
		FRotator rot = UKismetMathLibrary::FindLookAtRotation(GunMesh->GetComponentLocation(), CurrentEnemy->GetActorLocation());
		FRotator interpRot = UKismetMathLibrary::RInterpTo(GunMesh->GetComponentRotation(), rot, GetWorld()->GetDeltaSeconds(), 100.0f);
		GunMesh->SetRelativeRotation(interpRot);

		Shoot();
	}
}
