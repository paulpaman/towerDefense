// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGeneratorTower.h"
#include "Tower.h"
APowerGeneratorTower::APowerGeneratorTower()
{

}

void APowerGeneratorTower::BeginPlay()
{
	Super::BeginPlay();
}

void APowerGeneratorTower::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (ATower* tower = Cast<ATower>(OtherActor))
	{
		tower->AttackSpeed *= AttackSpeedMultiplier;
	}
}

void APowerGeneratorTower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
	if (ATower* tower = Cast<ATower>(OtherActor))
	{
		tower->AttackSpeed /= AttackSpeedMultiplier;
	}
}

void APowerGeneratorTower::Upgrade()
{
	Super::Upgrade();
	AttackSpeedMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	CurrentLevel++;
}

