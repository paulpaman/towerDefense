// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"
#include "Components/StaticMeshComponent.h"
#include "EnemyAIController.h"
#include "TowerDefenseGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "HealthComponent.h"
// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	this->AddOwnedComponent(HealthComponent);
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();

	CurrentWaypoint = 0;
	//MoveToWaypoints();
	//UHealthComponent* healthCP = Cast<UHealthComponent>(HealthComponent);
	HealthComponent->OnEnemyDeath.AddDynamic(this, &AEnemyAI::Die);
}

void AEnemyAI::Destroyed()
{
	Super::Destroyed();
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	isDead = false;
}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemyAI::IncreaseMaxHealth(float numMultiplier)
{
	HealthComponent->IncreaseHealth(numMultiplier);
}

void AEnemyAI::Die()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("EnemyDead!"));
	Cast<AEnemyAIController>(GetController())->DisableControlllerInput();
	StaticMesh->SetVisibility(false);
	OnDeath.Broadcast(this);
	/*if (isDead == true)
	{
		
	}*/
}

void AEnemyAI::MoveToWaypoints()
{
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());
	if (EnemyAIController)
	{
		if (CurrentWaypoint <= Waypoints.Num() - 1)
		{
			EnemyAIController->MoveToActor(Waypoints[CurrentWaypoint], 5.0f, false,true);
		}
	}
}

