// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

USTRUCT(BlueprintType)
struct FUpgradeConfig
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> UpgradeMultiplier;
};

UCLASS()
class TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
private:
	
protected:
	ATower();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* BaseMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* GunMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UArrowComponent* ProjectileSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* AttackRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UMaterial*> UpgradeMat;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AEnemyAI*> EnemiesInRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Upgrade")
		FUpgradeConfig UpgradeData;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class AEnemyAI* CurrentEnemy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> Projectile;

	UFUNCTION()
		virtual void SelectNearestTarget();

	UFUNCTION()
		virtual void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void CustomOnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION(BlueprintImplementableEvent)
		void OnClickedEvent(ATower* Tower);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Description")
		FString TowerDescription;

public:	

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 TotalCost;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 CurrentLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AttackSpeed;

	UFUNCTION(BlueprintCallable)
		virtual void Upgrade();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATowerNode* nodeOwner;

	//UFUNCTION(BlueprintCallable)
	//	virtual void Shoot();
};
