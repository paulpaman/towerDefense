// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowBuff.h"
#include "EnemyAI.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BuffAPI.h"

USlowBuff::USlowBuff()
{

}

void USlowBuff::BeginPlay()
{
	enemy->GetCharacterMovement()->MaxWalkSpeed /= SlowMultiplier;
}

void USlowBuff::RemoveBuff()
{
	enemy->GetCharacterMovement()->MaxWalkSpeed *= SlowMultiplier;

	this->DestroyComponent();
}
