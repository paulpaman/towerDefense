// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "PowerGeneratorTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APowerGeneratorTower : public ATower
{
	GENERATED_BODY()
private:
	FTimerHandle TimerHandle;
protected:
	APowerGeneratorTower();
	void BeginPlay() override;

	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void Upgrade() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AttackSpeedMultiplier;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class ATower*> TowersInRange;
};
