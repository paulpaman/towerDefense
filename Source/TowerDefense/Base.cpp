// Fill out your copyright notice in the Description page of Project Settings.


#include "Base.h"
#include "Components/StaticMeshComponent.h"
#include "EnemyAI.h"
#include "CoreHp.h"
// Sets default values
ABase::ABase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	SetRootComponent(BaseMesh);
}

// Called when the game starts or when spawned
void ABase::BeginPlay()
{
	Super::BeginPlay();
	

}

// Called every frame
void ABase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BaseMesh->OnComponentHit.AddDynamic(this, &ABase::OnHit);
}


void ABase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Hit!"));
		OnTrigger(enemy);
		enemy->isDead = true;
	}
}


