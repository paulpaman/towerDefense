// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthSignature);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()



protected:
	UHealthComponent();
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
		float CurrentHealth;

	UFUNCTION()
		void OnDeathEvent();
	
public:
	UFUNCTION()
		void TakeDamage(float damageTaken);

	UFUNCTION()
		void IncreaseHealth(float multiplier);

	UPROPERTY(BlueprintAssignable,Category = "Custom")
		FHealthSignature OnEnemyDeath;
};
