// Fill out your copyright notice in the Description page of Project Settings.


#include "Turret.h"
#include "Tower.h"
#include "EnemyAI.h"
#include "Projectile.h"
#include "HomingProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
ATurret::ATurret()
{
}

void ATurret::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATurret::AimAtTarget, AttackSpeed, true);
}

void ATurret::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		EnemiesInRange.Add(enemy);
		if (CurrentEnemy == nullptr)
		{
			SelectNearestTarget();
		}
	}
}

void ATurret::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		EnemiesInRange.Remove(enemy);
		SelectNearestTarget();
	}
}

void ATurret::SelectNearestTarget()
{
	Super::SelectNearestTarget();
}

void ATurret::Upgrade()
{
	Super::Upgrade();
	AttackSpeed *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	DamageMultiplier *= UpgradeData.UpgradeMultiplier[CurrentLevel];
	CurrentLevel++;
}

void ATurret::Shoot()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	if (CurrentEnemy != nullptr)
	{
		AProjectile* projectile = GetWorld()->SpawnActor<AHomingProjectile>(Projectile, ProjectileSpawn->GetComponentTransform(), spawnParams);
		projectile->Damage *= DamageMultiplier;
		projectile->SetHomingTarget(CurrentEnemy);
	}
}

void ATurret::AimAtTarget()
{
	if (CurrentEnemy != nullptr)
	{
		FRotator rot = UKismetMathLibrary::FindLookAtRotation(GunMesh->GetComponentLocation(), CurrentEnemy->GetActorLocation());
		FRotator interpRot = UKismetMathLibrary::RInterpTo(GunMesh->GetComponentRotation(), rot, GetWorld()->GetDeltaSeconds(), 100.0f);
		GunMesh->SetRelativeRotation(interpRot);

		Shoot();
	}

}


